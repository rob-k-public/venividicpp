#include <iostream>
#include <vector>
#include "shapes/Square.h"
#include "shapes/Circle.h"
#include "shapes/Triangle.h"
#include "canvas/Canvas.h"

int main() {
    std::cout << "JUST SOME SHAPES" << std::endl;
    auto square = Square(5);
    auto circle = Circle(5);
    auto triangle = Triangle(5, 6);
    std::cout << "Square: Cicrumference is " << square.getCircumference() << ", Surface is " << square.getSurface() << std::endl;
    std::cout << "Circle: Cicrumference is " << circle.getCircumference() << ", Surface is " << circle.getSurface() << std::endl;
    std::cout << "Triangle: Cicrumference is " << triangle.getCircumference() << ", Surface is " << triangle.getSurface() << std::endl;

    std::cout << "JUST A CANVAS WITH SOME SHAPES" << std::endl;
    auto canvas = Canvas();
    auto square2 = new Square(5);
    canvas.addShape(new Triangle(5, 6));
    canvas.addShape(square2);
    canvas.addShape(new Circle(5));
    std::cout << "Total circumference is " << canvas.getTotalCircumference() << ", total Surface is " << canvas.getTotalSurface() << std::endl;
    square2->increaseBase();
    std::cout << "Total circumference is " << canvas.getTotalCircumference() << ", total Surface is " << canvas.getTotalSurface() << std::endl;

    std::cout << "JUST MODIFYING SOME SHAPES" << std::endl;
    auto t1 = Triangle(1,2);
    auto t2 = Triangle(3,4);
    std::cout << "Triangle1: (" << t1.getBase() << ", " << t1.getHeight() << "), Triangle2: (" << t2.getBase() << ", " << t2.getHeight() << ")" << std::endl;
    auto t3 = t1 + t2;
    std::cout << "Triangle3: (" << t3.getBase() << ", " << t3.getHeight() << ")" << std::endl;

    std::cout << "SIZEOF THE WHOLE THING" << std::endl;
    std::cout << "sizeof canvas: " << sizeof(canvas) << std::endl;
    std::cout << "sizeof t3: " << sizeof(t3) << std::endl;
    std::cout << "sizeof triangle: " << sizeof(triangle) << std::endl;

    return 0;
}