//
// Created by rob on 03/06/17.
//

#ifndef MYFIRSTPROJECT_CANVAS_H
#define MYFIRSTPROJECT_CANVAS_H


#include <vector>
#include "../shapes/Shape.h"

class Canvas {

private:
    std::vector<Shape*>shapes;

public:
    long getTotalCircumference();
    long getTotalSurface();
    void addShape(Shape* shape);

    Canvas();

    virtual ~Canvas();

};


#endif //MYFIRSTPROJECT_CANVAS_H
