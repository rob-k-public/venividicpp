//
// Created by rob on 03/06/17.
//

#include "Canvas.h"

Canvas::Canvas() {}

Canvas::~Canvas() {

}

long Canvas::getTotalCircumference() {
    long totalCircumference = 0;
    for ( int i = 0 ; i < shapes.size() ; i++ )
       totalCircumference += shapes[i]->getCircumference();
    return totalCircumference;
}

long Canvas::getTotalSurface() {
    long totalSurface = 0;
    for ( int i = 0 ; i < shapes.size() ; i++ )
        totalSurface += shapes[i]->getSurface();
    return totalSurface;
}

void Canvas::addShape(Shape *shape) {
    shapes.push_back(shape);
}
