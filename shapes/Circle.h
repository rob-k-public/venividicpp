//
// Created by rob on 03/06/17.
//

#ifndef MYFIRSTPROJECT_CIRCLE_H
#define MYFIRSTPROJECT_CIRCLE_H


#include "Shape.h"

class Circle : public Shape {
private:
    int radius;

public:
    Circle(int r);
    long getCircumference();
    long getSurface();

    virtual ~Circle();

};



#endif //MYFIRSTPROJECT_CIRCLE_H
