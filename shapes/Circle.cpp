//
// Created by rob on 03/06/17.
//

#include <math.h>
#include "Circle.h"

Circle::Circle(int r) {
    radius = r;
}

long Circle::getCircumference() {
    return (long) (M_PI * pow(radius, 2));
}

long Circle::getSurface() {
    return (long) (M_PI * radius * 2);
}

Circle::~Circle() {

}
