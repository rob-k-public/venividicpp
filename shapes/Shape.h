//
// Created by rob on 03/06/17.
//

#ifndef MYFIRSTPROJECT_SHAPE_H
#define MYFIRSTPROJECT_SHAPE_H


class Shape {

public:

    virtual long getCircumference() = 0;
    virtual long getSurface() =0;

};

#endif //MYFIRSTPROJECT_SHAPE_H
