//
// Created by rob on 03/06/17.
//

#include "Square.h"

Square::Square(int b) {
    base = b;
}

Square::~Square() {

}

long Square::getCircumference() {
    return base * 4;
}

long Square::getSurface() {
    return base * base;
}

void Square::increaseBase(int size) {
    base += size;
}
