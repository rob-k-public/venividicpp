//
// Created by rob on 03/06/17.
//

#ifndef MYFIRSTPROJECT_TRIANGLE_H
#define MYFIRSTPROJECT_TRIANGLE_H


#include "Shape.h"

class Triangle : public Shape {
private:
    int base;
    int height;

public:
    long getCircumference();
    long getSurface();
    Triangle operator+(const Triangle& triangle);
    int getBase();
    int getHeight();
    Triangle(int base, int height);

    virtual ~Triangle();

};


#endif //MYFIRSTPROJECT_TRIANGLE_H
