//
// Created by rob on 03/06/17.
//

#include <cmath>
#include "Triangle.h"

Triangle::Triangle(int base, int height) : base(base), height(height) {}

Triangle::~Triangle() {

}

long Triangle::getSurface() {
    return (base * height)/2;
}

long Triangle::getCircumference() {
    double c = sqrt(pow(base,2) + pow(height, 2));
    return (long) (base + height + c);
}

Triangle Triangle::operator+(const Triangle &triangle) {
    return Triangle(this->base + triangle.base, this->height + triangle.height);
}

int Triangle::getHeight() {
    return height;
}

int Triangle::getBase() {
    return base;
}
