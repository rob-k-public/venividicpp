//
// Created by rob on 03/06/17.
//

#ifndef MYFIRSTPROJECT_SQUARE_H
#define MYFIRSTPROJECT_SQUARE_H

#include "Shape.h"

class Square : public Shape {
private:
    int base;

public:
    Square(int base);
    long getCircumference();
    long getSurface();
    void increaseBase(int size = 5);

    virtual ~Square();

};

#endif //MYFIRSTPROJECT_SQUARE_H
